#coding: utf-8

import factory

from flats.factories import FlatOptionFactory

import topflats


class RawDataFlatFactory(factory.DjangoModelFactory):
    class Meta:
        model = topflats.models.RawDataFlat
    flat_option = factory.SubFactory(FlatOptionFactory)
