#encode: utf-8

from django.conf.urls import url, include
from django.contrib import admin

from topflats import views as topflats_view

urlpatterns = [
    url(r'top/$', topflats_view.top_flats, name="top"),
    url(r'top/html/$', topflats_view.top_html, name="top_html"),
    url(r'top/month/(?P<year>[0-9]{4})/(?P<month>[0-9]{1,2})/$', topflats_view.top_flats_by_month, name="top_by_month")
]
