#coding: utf-8

from datetime import datetime

from django.utils.translation import ugettext as _
from django.http import JsonResponse

from topflats.models import RawDataFlat


def is_valid_filter(month):
    if month > 12 or month < 1:
        return False
    return True


def top_flats(request):
    response_data = {
        'message': _("Sucess"),
        'result': {'flats': None, 'carrier': None}
    }
    try:
        topflats = RawDataFlat.top()
        topcarrier = RawDataFlat.top_carrier(top_flats=topflats)
        response_data['result']['flats'] = list(topflats)
        response_data['result']['carrier'] = list(topcarrier)
    except ValueError as error:
        message = _(u"Erro trying retrive measurements: %s" % error)
        response_data.update({
            'message': message,
            'result': None
        })
        return JsonResponse(response_data, status=500)
    return JsonResponse(response_data, status=200)

def top_flats_by_month(request, year, month):
    response_data = {
        'message': '',
        'result': {'flats': None, 'carrier': None}
    }
    month = int(month)
    year = int(year)

    if not is_valid_filter(month):
        message = _(u"Filter passed was invalid for this operation")
        response_data.update({
            'message': message,
            'result': {'flats': None, 'carrier': None}
        })
        return JsonResponse(response_data, status=400)
    now = datetime.now().date().replace(day=1, month=month, year=year).strftime(RawDataFlat.date_format)
    try:
        topflats = RawDataFlat.top_by_month(date=now)
        topcarrier = RawDataFlat.top_carrier(top_flats=topflats)
        response_data['result']['flats'] = list(topflats)
        response_data['result']['carrier'] = list(topcarrier)
    except ValueError as error:
        message = _(u"Erro trying retrive measurements: %s" % error)
        response_data.update({
            'message': message,
            'result': None
        })
        return JsonResponse(response_data, status=500)
    return JsonResponse(response_data, status=200)

def top_html(request):
    from django.shortcuts import render_to_response
    return render_to_response('index.html', context={})
