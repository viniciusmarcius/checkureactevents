#encode: utf-8

import mock

from django.test import TestCase
from django.test import Client
from django.core.urlresolvers import reverse

from topflats.views import top_flats_by_month, is_valid_filter
from topflats.models import RawDataFlat

#
# class TopFlatsTestCase(TestCase):
#     def test_is_valid_filter(self):
#         self.assertTrue(is_valid_filter(filters={'date': '2015-10-1'}))
#
#     def test_not_is_valid_filter(self):
#         self.assertFalse(is_valid_filter(filters={'date': 'Not a Date'}))
#
#     @mock.patch.object(RawDataFlat, 'list')
#     @mock.patch.object(RawDataFlat, 'get_from_ureact')
#     def test_no_fiter(self, m_list_flats, m_ureact):
#         params = {'date': ''}
#         client = Client()
#         m_list_flats.return_value = None
#
#         response = client.post(reverse('top_flats'), params)
#         expected_data = [
#             {
#                 'id': 1,
#                 'name': u'Plain Free 400',
#                 'carrier': {
#                     'id': 1,
#                     'name': 'Carrier One'
#                 },
#                 'count_clicks': 10
#             },
#             {
#                 'id': 2,
#                 'name': u'Plain Free 100',
#                 'carrier': {
#                     'id': 2,
#                     'name': 'Carrier One'
#                 },
#                 'count_clicks': 8
#
#             },
#             {
#                 'id': 3,
#                 'name': u'Plain Free 200',
#                 'carrier': {
#                     'id': 3,
#                     'name': 'Carrier One'
#                 },
#                 'count_clicks': 5
#
#             }
#         ]
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(response.data, expected_data)
#         m_ureact.assert_called_with(date=date(20016, 1, 1), flats=None)
