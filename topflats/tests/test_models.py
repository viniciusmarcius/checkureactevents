#encode: utf-8

import mock
from freezegun import freeze_time
from datetime import date
from django.utils.translation import ugettext as _
from django.test import TestCase
from topflats.models import RawDataFlat
from topflats.factories import RawDataFlatFactory
from flats.models import FlatOption
from flats.factories import FlatOptionFactory, CarrierFactory


class RawDataFlatTest(TestCase):

    @mock.patch.object(FlatOption.objects, 'get')
    def test_format_to_db(self, m_flat_get):
        fake_metrics = [
            {
            u'avg_value': 15.0,
            u'count': 1,
            u'metric_label': 1,
            u'day': u'2016-05-19',
            u'max_value': 15.0,
            u'metric': u'click',
            u'min_value': 0.0,
            u'sum_value': 15.0
            },
            {
            u'avg_value': 15.0,
            u'count': 2,
            u'metric_label': 1,
            u'day': u'2016-05-20',
            u'max_value': 15.0,
            u'metric': u'click',
            u'min_value': 0.0,
            u'sum_value': 15.0
            }
        ]
        flat_opt1 = FlatOptionFactory(name="Flat One")
        flat_opt2 = FlatOptionFactory(name="Flat Two")
        m_flat_get.return_value = flat_opt1
        RawDataFlat.format_to_db(fake_metrics)
        instances = RawDataFlat.objects.all()
        self.assertEqual(len(instances), 1)
        self.assertEqual(instances[0].count_clicks, 3)

    @mock.patch.object(FlatOption.objects, 'get')
    def test_format_to_db_diferents_dates(self, m_flat_get):
        fake_metrics = [
            {
            u'avg_value': 15.0,
            u'count': 1,
            u'metric_label': 1,
            u'day': u'2016-05-19',
            u'max_value': 15.0,
            u'metric': u'click',
            u'min_value': 0.0,
            u'sum_value': 15.0
            },
            {
            u'avg_value': 15.0,
            u'count': 2,
            u'metric_label': 1,
            u'day': u'2016-05-20',
            u'max_value': 15.0,
            u'metric': u'click',
            u'min_value': 0.0,
            u'sum_value': 15.0
            },
            {
            u'avg_value': 15.0,
            u'count': 2,
            u'metric_label': 1,
            u'day': u'2016-06-20',
            u'max_value': 15.0,
            u'metric': u'click',
            u'min_value': 0.0,
            u'sum_value': 15.0
            }
        ]
        flat_opt1 = FlatOptionFactory(name="Flat One")
        flat_opt2 = FlatOptionFactory(name="Flat Two")
        m_flat_get.return_value = flat_opt1
        RawDataFlat.format_to_db(fake_metrics)
        instances = RawDataFlat.objects.all()
        self.assertEqual(len(instances), 2)
        self.assertEqual(instances[0].count_clicks, 3)
        self.assertEqual(instances[1].count_clicks, 2)

    @mock.patch.object(RawDataFlat, 'log')
    def test_format_to_db_plan_does_not_exist(self, m_log):
        fake_metrics = [
            {
                u'avg_value': 15.0,
                u'count': 1,
                u'metric_label': 3,
                u'day': u'2016-05-19',
                u'max_value': 15.0,
                u'metric': u'click',
                u'min_value': 0.0,
                u'sum_value': 15.0
            },
            {
                u'avg_value': 15.0,
                u'count': 2,
                u'metric_label': 3,
                u'day': u'2016-05-20',
                u'max_value': 15.0,
                u'metric': u'click',
                u'min_value': 0.0,
                u'sum_value': 15.0
            }
        ]
        RawDataFlat.format_to_db(fake_metrics)
        m_log.assert_called_with(_("FlatOption not found: FlatOption matching query does not exist."))

    @mock.patch.object(RawDataFlat.client, 'get_statistics')
    def test_get_from_ureact(self, m_get_statistics):
        date = '2016-6-1'
        RawDataFlat.get_in_ureact(date)
        m_get_statistics.assert_called_with(['2016-06-01', '2016-06-30'])

    @freeze_time('2016-07-22')
    @mock.patch.object(RawDataFlat.objects, 'filter')
    def test_top_without_params(self, m_rawdata_filter):
        RawDataFlat.top()
        m_rawdata_filter.assert_called_with(
            date__range=[date(2016, 07, 1),
                         date(2016, 07, 31)])

    def test_top_carrier(self):
        carrier_1 = CarrierFactory.create(name="Carrier1")
        carrier_2 = CarrierFactory.create(name="Carrier2")
        flat_1 = FlatOptionFactory.create(name="Flat1", carrier=carrier_1)
        flat_2 = FlatOptionFactory.create(name="Flat2", carrier=carrier_2)
        raw_data_1 = RawDataFlatFactory.create(count_clicks=10, flat_option=flat_2, date=date(2016, 1, 1))
        raw_data_2 = RawDataFlatFactory.create(count_clicks=7, flat_option=flat_2, date=date(2016, 2, 1))
        raw_data_3 = RawDataFlatFactory.create(count_clicks=5, flat_option=flat_2, date=date(2016, 3, 1))
        raw_data_4 = RawDataFlatFactory.create(count_clicks=4, flat_option=flat_1, date=date(2016, 2, 1))
        expected_top_carrier = [
            {
                u'name': u"Carrier2",
                'total_click': 22
            },
            {
                u'name': u"Carrier1",
                'total_click': 4
            }
        ]
        top_carrier = RawDataFlat.top_carrier()
        self.assertEqual(top_carrier[0], expected_top_carrier[0])
        self.assertEqual(top_carrier[1], expected_top_carrier[1])
