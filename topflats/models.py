#encode: utf-8

from __future__ import unicode_literals

import calendar
from datetime import datetime
from itertools import ifilter

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _

from ureactme import Client
from flats.models import FlatOption, Carrier


class RawDataFlat(models.Model):
    client = Client(settings.UREACTME_KEY)
    date_format = '%Y-%m-%d'
    fields = ('date', 'count_clicks', 'flat_option__name', 'flat_option__carrier__name')

    flat_option = models.ForeignKey(FlatOption)
    date = models.DateField()
    count_clicks = models.IntegerField()

    class Meta:
        #TODO: TEST THIS
        unique_together = (("flat_option", "date"),)

    def __unicode__(self):
        return u"%s-%s" % (self.flat_option, self.date)

    @classmethod
    def format_to_db(cls, metrics):
        for metric in metrics:
            try:
                flat_id = metric.get('metric_label', None)
                flat = FlatOption.objects.get(pk=flat_id)
                day = metric.get('day')
                date = datetime.strptime(day, cls.date_format)
                date = date.replace(day=1).date()
                count = metric.get('count', 1)
                raw, created = cls.objects.get_or_create(flat_option=flat,
                                                         date=date,
                                                         defaults={'count_clicks':count})
                if not created:
                    raw.count_clicks = models.F('count_clicks') + count
                    raw.save()
            except FlatOption.DoesNotExist as e:
                cls.log(_("FlatOption not found: %(error)s") % {'error': e})
                continue
            except ValueError as e:
                cls.log(_("Raised an error: %(error)s") % {'error': e})
                continue

    @classmethod
    def get_in_ureact(cls, date):
        metrics = []
        date = datetime.strptime(date, cls.date_format)
        last_day = calendar.monthrange(date.year, date.month)[1]
        range_date = [date.strftime(cls.date_format), date.replace(day=last_day).strftime(cls.date_format)]
        d_metrics = cls.client.get_statistics(range_date)
        if not d_metrics:
            return []
        for m in d_metrics.objects[1:]:
            metrics.append(dict(zip(d_metrics.objects[0], m)))
        if not metrics:
            return None
        click_metrics = ifilter(lambda x: x.get('metric_action') == 'click', metrics)
        cls.format_to_db(click_metrics)
        return RawDataFlat.objects.filter(date__range=range_date)

    @classmethod
    def top_carrier(cls, top_flats=None):
        if top_flats:
            filter_carrier = [x['flat_option__carrier__name'] \
                 for x in top_flats]
            top_carrier = Carrier.objects.filter(name__in=filter_carrier)
        else:
            top_carrier = Carrier.objects.all()
        return top_carrier.values('name')\
            .order_by('-total_click')\
            .annotate(
                total_click=models.Sum(
                    models.F('flatoption__rawdataflat__count_clicks')
                )
            )

    @classmethod
    def top_by_month(cls, date):
        values = RawDataFlat.objects.filter(date=date)
        if not values:
            values = cls.get_in_ureact(date)
        return values.order_by('-count_clicks').values(*cls.fields) or None

    @classmethod
    def top(cls):
        now = datetime.now()
        first_day = now.replace(day=1).date()
        last_day = calendar.monthrange(now.year, now.month)[1]
        now_last_day = now.replace(day=last_day).date()
        range_date = [first_day, now_last_day]
        values = RawDataFlat.objects.filter(date__range=range_date)
        return values.order_by('-count_clicks').values(*cls.fields) or None

    @classmethod
    def log(cls, message):
        print(message)
