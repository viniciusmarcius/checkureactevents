var chartFlats, chartCarrier;

function getMetric(url){
    $.ajax({
      url: url,
      context: document.body,
      dataType: "json",
    }).success(function(response) {
        drawChart(response.result);
    }).error(function(response){
        drawChart([{'flat_option__name': '', 'count_clicks': 0}]);
        $('#errormessage').html("No data found for thist month!");
        $('#alertmessage').show();
    });
};

function drawChart(data){
    if (chartFlats && chartCarrier){
        chartFlats.setData(data.flats);
        chartCarrier.setData(data.carrier);
    } else {
        chartFlats = Morris.Bar({
            data: data.flats,
            element: 'chartFlats',
            xkey: 'flat_option__name',
            ykeys: ['count_clicks'],
            labels: ['Total Clicks']
        });

        chartCarrier = Morris.Bar({
            data: data.carrier,
            element: 'chartCarrier',
            xkey: 'name',
            ykeys: ['total_click'],
            labels: ['Total Clicks']
        });
    }
};

$(document).ready(function () {
    $('.form-control').datepicker({
        format: "mm-yyyy",
        viewMode: "months",
        minViewMode: "months"
     }).on('changeDate', function (ev) {
        var month = ev.date.getMonth() + 1,
            year = ev.date.getFullYear();
        getMetric("/flats/top/month/" + year + "/" + month);
    });
    $('.alert').alert();
    getMetric("/flats/top/");
});
