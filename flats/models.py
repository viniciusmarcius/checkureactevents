#coding: utf-8

from __future__ import unicode_literals
from django.db import models


class Carrier(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class FlatOption(models.Model):
    name = models.CharField(max_length=255)
    carrier = models.ForeignKey(Carrier)

    def __unicode__(self):
        return self.name
