#coding: utf-8

import factory

import flats

class CarrierFactory(factory.DjangoModelFactory):
    class Meta:
        model = flats.models.Carrier

class FlatOptionFactory(factory.DjangoModelFactory):
    class Meta:
        model = flats.models.FlatOption
    carrier = factory.SubFactory(CarrierFactory)
