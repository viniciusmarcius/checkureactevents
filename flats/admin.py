from django.contrib import admin

from flats.models import Carrier, FlatOption

admin.site.register(Carrier)
admin.site.register(FlatOption)
